package user;

import java.io.Serializable;
import java.lang.String;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Persistence;
import javax.persistence.Query;


/**
 * Entity implementation class for Entity: User
 *
 */
@Entity
@NamedQuery(
		name = "FindUserFerdy245",
		query = "SELECT u FROM User u WHERE u.nick = 'ferdy245'")

@ManagedBean
@SessionScoped
public class User implements Serializable {

	@Id @GeneratedValue
	private Long id;
	
	private String nick;
	private String password;
	private String mail;
	private String fullName;
	private static final long serialVersionUID = 1L;

	public User() {
		super();
	} 
	
	public User(String nick, String password, String mail, String fullName){
		
		this.nick = nick;
		this.password = password;
		this.mail = mail;
		this.fullName = fullName;
	}
	
	public String getNick() {
		return this.nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}   
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}   
	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}   
	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
   
	public String addUser(){
 		EntityManagerFactory emf = Persistence.createEntityManagerFactory("PracticoTSI2");
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
 		em.persist(this);
 		em.getTransaction().commit();
		return "success";
	}
	
	public static User findUser(String nick, String password){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("PracticoTSI2");
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT u FROM User u WHERE u.nick = '" + nick + "' and u.password = '" + password + "'", User.class);
        return (User) q.getSingleResult();
    }

}
