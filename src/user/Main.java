package user;

import javax.persistence.*;



public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		User u = new User("ferdy245", "123456", "fer@mail.com", "Fernanda Toledo");
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("PERSISTENCE_CONTEXT_NAME");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		((EntityManager) tx).persist(u);
		tx.commit();
		
		Query query = em.createNamedQuery("FindUserFerdy245", User.class);
		u = (User) query.getSingleResult();
		em.close();
		emf.close();
		
		System.out.println("hello");
	}

}
